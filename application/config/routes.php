<?php
defined('BASEPATH') or exit('No direct script access allowed');
$route['default_controller'] = 'welcome';
$route['404_override'] = '';

// dashboard
$route['get/dashboard/getlist'] = 'dashboard/getdashboard';
$route['get/dashboard/sitemap'] = 'dashboard/sitemap';

// pages 
$route['get/pages/getlist'] = 'pages/getlist';
$route['get/pages/getlistsolution'] = 'pages/getlistsolution';
$route['get/pages/getrow'] = 'pages/getrow';
$route['get/pages/grouptype'] = 'pages/grouptype';
$route['set/pages/process'] = 'pages/process';
$route['set/pages/remove'] = 'pages/remove';
$route['set/pages/changestatus'] = 'pages/changestatus';
$route['set/pages/updateRelated'] = 'pages/updateRelated';

// grouppages
$route['get/pagesgroup/getlist'] = 'pagegroup/getlist';
$route['get/pagesgroup/getrow'] = 'pagegroup/getrow';
$route['set/pagesgroup/process'] = 'pagegroup/process';
$route['set/pagesgroup/remove'] = 'pagegroup/remove';

// product
$route['get/product/getlist'] = 'product/getlist';
$route['get/product/getrow'] = 'product/getrow';
$route['set/product/process'] = 'product/process';
$route['set/product/remove'] = 'product/remove';
$route['set/product/updateImages'] = 'product/updateImages';
$route['set/product/updateSeo'] = 'product/updateSeo';
$route['set/product/updatePrice'] = 'product/updatePrice';
$route['set/product/updateAttribute'] = 'product/updateAttribute';
$route['set/product/changeStatus'] = 'product/changeStatus';

// contact
$route['get/contact/getlist'] = 'contact/getlist';
$route['get/contact/getrow'] = 'contact/getrow';
$route['set/contact/process'] = 'contact/process';
$route['set/contact/remove'] = 'contact/remove';
$route['set/contact/sendmail'] = 'contact/sendmailContact';

// content
$route['get/content/getlist'] = 'content/getlist';
$route['get/content/getrow'] = 'content/getrow';
$route['set/content/process'] = 'content/process';
$route['set/content/remove'] = 'content/remove';
$route['set/content/changepin'] = 'content/changepin';
$route['set/content/updateRelated'] = 'content/updateRelated';


// partner 
$route['get/partner/getlist'] = 'partner/getlist';
$route['get/partner/getrow'] = 'partner/getrow';
$route['set/partner/process'] = 'partner/process';
$route['set/partner/remove'] = 'partner/remove';

// personnel
$route['get/user/getlist'] = 'user/getlist';
$route['get/user/getrow'] = 'user/getrow';
$route['set/user/process'] = 'user/process';
$route['set/user/remove'] = 'user/remove';
$route['set/user/changepassword'] = 'user/changepassword';
$route['set/user/changeToken'] = 'user/changeToken';

//settings
$route['get/settings/emailgetrow'] = 'settings/emailgetrow';
$route['set/settings/email'] = 'settings/email';
$route['get/settings/getorther'] = 'settings/getorther';
$route['set/settings/setorther'] = 'settings/setorther';
$route['get/settings/settingAdmin'] = 'settings/settingAdmin';

$route['get/title/getrowcompany'] = 'title/getrowcompany';
$route['set/title/company'] = 'title/company';
$route['get/slide/getlist'] = 'slide/getlist';
$route['get/slide/getrow'] = 'slide/getrow';
$route['set/slide/process'] = 'slide/process';
$route['set/slide/remove'] = 'slide/remove';

$route['api/getProductByIdGroup'] = 'api/getProductByIdGroup';

// Home
$route['api/getProductGroup'] = 'api/getProductGroup';
$route['api/getProductList'] = 'api/getProductList';
$route['api/getAboutUs'] = 'api/getAboutUs';
$route['api/getContentHome'] = 'api/getContent';
$route['api/getPartner'] = 'api/getPartner';
$route['api/getProductGroupHome'] = 'api/getProductGroupHome';

// content
$route['api/getContent'] = 'api/getContent';

// api product
$route['api/getProduct'] = 'api/getProduct';
$route['api/productCategory'] = 'api/productCategory';
$route['api/getProductDetail'] = 'api/getProductDetail';
$route['api/productGroup'] = 'api/productGroup';

// api getPage
$route['api/getDetailPagesByLink'] = 'api/getDetailPagesByLink';

// add contact
$route['api/addcontact'] = 'api/addContact';

// api content 
$route['api/getContentDetail'] = 'api/getContentDetail';
$route['api/getContentCategory'] = 'api/getContentCategory';

/* SEARCH */
$route['api/search'] = 'api/search';

// get slide
$route['api/home/slide'] = 'api/getslide';

// brand
$route['get/brand/getlist'] = 'brand/getlist';

// origin
$route['get/origin/getlist'] = 'origin/getlist';

// Api language
$route['api/setting/language'] = 'api/setting';

$route['api/getmenu'] = 'api/getmenu';

$route['api/company'] = 'api/company';

$route['api/processApi'] = 'api/processApi';
/**
 *
 * Create
 * Name: PHAN ANH VAN
 * Date: 16/03/2021
 * Note: API SETTINGS WEBSITE
 * ------------------
 * Edit
 * Name:
 * Date:
 * Note:
 */
$route['api/contact/add'] = 'api/addContact';

// api
$route['api/logoutadmin'] = 'api/logoutadmin';
$route['api/checklogin'] = 'api/checklogin';
$route['api/admin/login'] = "api/adminLogin";
$route['(.*)'] = $route['default_controller'];
$route['translate_uri_dashes'] = FALSE;
