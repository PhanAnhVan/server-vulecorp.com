<?php 
if(!function_exists("checkTypeDate")){
	function checkTypeDate($date)
    {
    	$d = explode("-", $date);
    	$skip = false;
    	if(count($d) == 3){
			$skip = checkdate($d[1],$d[2],$d[0]);
    	}
    	return $skip;
    }
}
if(!function_exists("checkTypeNumber")){
	function checkTypeNumber($num)
    {
    	return is_numeric($num) ? true : false;
    }
}
if(!function_exists("processMessage")){
	function processMessage($message,$data)
    {
    	$message = preg_replace( "/\r|\n/", "", $message);
		foreach($data as $key => $value){
			$search ="{{".$key."}}";
			$message = str_replace($search, $value, $message);
		}
		return $message;
    }
}
if(!function_exists("getByHostName")){
	function getByHostName($hostname)
    {
		$hostname = ltrim($hostname,'https://');
		$hostname = ltrim($hostname,'http://');
		$hostname = ltrim(ltrim($hostname,'www'),'.');
		$hostname = rtrim($hostname,'/');
		return $hostname;
	}
}
if(!function_exists("getIpConnect")){
	function getIpConnect()
    {
		return (!empty($_SERVER['HTTP_CLIENT_IP'])) ? $_SERVER['HTTP_CLIENT_IP'] : ((!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']);
    }
}
if(!function_exists("filedToken")){
	function filedToken()
    {
		return array('ipwebsite','domain_name','id','password');
	}
}
if(!function_exists("extractToken")){
	function extractToken($token)
    {
		$filed = filedToken();
		$data = decryptAES($token);
		$extract = explode("|",$data);
		if(count($extract) == count($filed)){
			$obj = array();
			for($i = 0 ; $i < count($extract) ;$i++){
				$obj[$filed[$i]] = $extract[$i];
			}
			return $obj;
		}else{
			return null;
		}
    }
}
if(!function_exists("createToken")){
	function createToken($data)
    {
		$token = null;
		$filed = filedToken();
		$type = (gettype($data) ==='object') ? 0 : ((gettype($data) ==='array') ? 1 : -1);
		if($type > -1){
			for($i = 0 ;$i < count($filed);$i++){
				if($type == 0){
					if(property_exists($data,$filed[$i])){
						$token.=$data->$filed[$i];
					}else{
						return null;
					}
				}else{
					if(array_key_exists($filed[$i],$data)){
						$token.=$data[$filed[$i]];
					}else{
						return null;
					}
				}
				$token.= ($i == count($filed) - 1) ? "" : "|";
			}
		}
		if($token!=null){
			$token = encryptAES($token);
		}
		return $token;
	}
}
if(!function_exists("encryptAES")){
	function encryptAES($val)
    {
		$keyNow = strtotime("now");
		$key = hash("SHA256", $keyNow, true);
		$encrypt_string = rtrim(rtrim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256,$key, $val.$keyNow,MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256,MCRYPT_MODE_ECB),MCRYPT_RAND))), "\0"),"==");
		$numAuto = rand(0,strlen($encrypt_string));
		return substr_replace($encrypt_string,KEYTOKEN.$keyNow,$numAuto,0);
	}
}
if(!function_exists("decryptAES")){
	function decryptAES($val)
    {	
		$keyNow = substr($val,strpos($val,KEYTOKEN) + strlen(KEYTOKEN),10);
		$key =  hash("SHA256", $keyNow, true);
		$val = substr($val,0,strpos($val,KEYTOKEN.$keyNow)).substr($val,strpos($val,KEYTOKEN.$keyNow)+ strlen(KEYTOKEN.$keyNow), strlen($val)).'==';
		$decrypt_string = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256,$key,base64_decode($val),MCRYPT_MODE_ECB,mcrypt_create_iv(mcrypt_get_iv_size( MCRYPT_RIJNDAEL_256,MCRYPT_MODE_ECB),MCRYPT_RAND)), "\0");
		
		return $decrypt_string;//substr($decrypt_string,0,-strlen($keyNow));
	}
}
/////////
if(!function_exists("scanLottery")){
	function scanLottery($Url,$date)
    {
		$html = file_get_contents($Url.$date);
		//echo $html;die;
		$doc = new DOMDocument();
		@$doc->loadHTML($html);

		$xpath = new DOMXpath($doc);
		$key = "kqxs_1-".$date;

		$entries = $xpath->query("//div[@id='".$key."']//div[@class='kqxs_content']//table[@class='tblKQTinh']");
		$data = array();
		$className = array('giai_tam'=>'giai-tam','giai_bay'=>'giai-bay','giai_sau'=>'giai-sau','giai_nam'=>'giai-nam','giai_tu'=>'giai-tu','giai_ba'=>'giai-ba','giai_nhi'=>'giai-nhi','giai_nhat'=>'giai-nhat','giai_dac_biet'=>'giai-dac-biet');

		foreach ($entries as $entry) {
			$ad_Doc = new DOMDocument();
			$cloned = $entry->cloneNode(TRUE);
    		$ad_Doc->appendChild($ad_Doc->importNode($cloned,true));
    		$vpath = new DOMXPath($ad_Doc);
  			$city = $vpath->query("//td[@class='tentinh']//span[@class='namelong']");
  			if($city->length > 0){
  				$cityName = $city->item(0)->nodeValue;
  				$keys = removesign($cityName);
  				if(!array_key_exists($keys, $data)){
					$data[$keys] = array('item'=>array(),'list'=>array());
				}
				$data[$keys]['item']['lotdate'] = $date;
				$data[$keys]['item']['city'] = ltrim(rtrim($keys));
  				$ticket = $vpath->query("//td[@class='loaive']");
				if($ticket->length > 0){
					$data[$keys]['item']['ticket_type'] = ltrim(rtrim($ticket->item(0)->nodeValue));
				}
				foreach ($className as $key => $value) {
					if(!array_key_exists($value, $data[$keys]['list'])){
						$data[$keys]['list'][$value] = array();
					}
					$element = $vpath->query("//td[@class='".$key."']//div");
					foreach ($element as $val) {
						array_push($data[$keys]['list'][$value], $val->nodeValue);
					}
				}
  			}
		}
		return $data;
	}
}
if(!function_exists("removesign")){

    function removesign($str)
    {
        $delimiter = '-';
		$str = vn_to_str($str);
        $clean = replace_keyword($str);
        $clean = replace_accent($clean);
        $clean = mb_convert_encoding($clean, 'UTF-8', 'ISO-8859-2');
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
        return $clean;
    }
}  
if(!function_exists("replace_accent")){
    function replace_accent($str) 
    { 
		$a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'Ð', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', '?', '?', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', '?', '?', 'L', 'l', 'N', 'n', 'N', 'n', 'N', 'n', '?', 'O', 'o', 'O', 'o', 'O', 'o', 'Œ', 'œ', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'Š', 'š', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Ÿ', 'Z', 'z', 'Z', 'z', 'Ž', 'ž', '?', 'ƒ', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', '?', '?', '?', '?', '?', '?'); 
		$b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o'); 
		return str_replace($a, $b, $str); 
    }
}
if(!function_exists("replace_keyword")){
    function replace_keyword($str)
    {
        $a=array("à","á","?","?","ã","â","?","?","?","?","?","a","?","?","?","?","?","è","é","?","?","?","ê","?","?","?","?","?","ì","í","?","?","i","ò","ó","?","?","õ","ô","?","?","?","?","?","o","?","?","?","?","?","ù","ú","?","?","u","u","?","?","?","?","?","?","ý","?","?","?","d","À","Á","?","?","Ã","Â","?","?","?","?","?","A","?","?","?","?","?","È","É","?","?","?","Ê","?","?","?","?","?","Ì","Í","?","?","I","Ò","Ó","?","?","Õ","Ô","?","?","?","?","?","O","?","?","?","?","?","Ù","Ú","?","?","U","U","?","?","?","?","?","?","Ý","?","?","?","Ð","ê","ù","à","Q","W","E","R","T","Y","U","I","O","P","A","S","D","F","G","H","J","K","L","Z","X","C","V","B","N","M");
        $b=array("a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","e","e","e","e","e","e","e","e","e","e","e","i","i","i","i","i","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","u","u","u","u","u","u","u","u","u","u","u","y","y","y","y","y","d","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","e","e","e","e","e","e","e","e","e","e","e","i","i","i","i","i","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","u","u","u","u","u","u","u","u","u","u","u","y","y","y","y","y","d","e","u","a","q","w","e","r","t","y","u","i","o","p","a","s","d","f","g","h","j","k","l","z","x","c","v","b","n","m");
        return str_replace($a, $b, $str);
    }
}
if(!function_exists("vn_to_str")){
	function vn_to_str ($str){
	 
		$unicode = array(
		 
		'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',
		 
		'd'=>'đ',
		 
		'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
		 
		'i'=>'í|ì|ỉ|ĩ|ị',
		 
		'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
		 
		'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
		 
		'y'=>'ý|ỳ|ỷ|ỹ|ỵ',
		 
		'A'=>'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
		 
		'D'=>'Đ',
		 
		'E'=>'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
		 
		'I'=>'Í|Ì|Ỉ|Ĩ|Ị',
		 
		'O'=>'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
		 
		'U'=>'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
		 
		'Y'=>'Ý|Ỳ|Ỷ|Ỹ|Ỵ',
		 
		);
		 
		foreach($unicode as $nonUnicode=>$uni){
		 
		$str = preg_replace("/($uni)/i", $nonUnicode, $str);
		 
		}
		$str = str_replace(' ','_',$str);
		 
		return $str;	 
	}
}